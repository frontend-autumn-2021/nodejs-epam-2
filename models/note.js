const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const noteScheme = new Schema(
    {
      userId: String,
      completed: Boolean,
      text: String,
      createdDate: Date,
    },
    {versionKey: false, collection: 'users.notes'});

module.exports = mongoose.model('Note', noteScheme);
