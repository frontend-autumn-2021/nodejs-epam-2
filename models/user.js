const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userScheme = new Schema(
    {
      username: String,
      password: String,
      createdDate: Date,
    }, {versionKey: false, collection: 'users'});

module.exports = mongoose.model('User', userScheme);
