const bcrypt = require('bcrypt');
require('dotenv').config();
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const app = require('../app');

exports.registerUser = async function (req, res) {
  try {
    if (!req.body) {
      throw new Error('Invalid value');
    }
    const username = req.body.username;
    const checkUser = await User.find({ username: username });
    if (checkUser.length) {
      throw new Error('User with this username is already registered');
    }
    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(req.body.password, salt);
    const date = new Date();
    let user = new User({
      username: username,
      password: password,
      createdDate: date,
    });

    user = await user.save();
    res.status(200).send({
      message: 'Success',
    });
    console.log(app.formattedTime() + 'Registered user: ' + user._id);
  } catch (err) {
    res.status(400).send({
      message: err.message
    });
    console.error(app.formattedTime() + err);
  }
};

exports.authUser = async function (req, res) {
  try {
    if (!req.body) {
      throw new Error('Invalid value');
    }
    const { username, password } = req.body;

    const user = await User.findOne({ username: username });

    if (!user) {
      throw new Error('User are not found');
    }
    const passwordResult = bcrypt.compareSync(password, user.password);
    if (!passwordResult) {
      throw new Error('Password is incorrect');
    }

    const token = jwt.sign(user.toJSON(), process.env.JWT_SECRET);

    res.status(200).json({
      message: 'Success',
      jwt_token: token,
    });
    console.log(app.formattedTime() + 'Login user: ' + user._id);
  } catch (err) {
    res.status(400).send({
      message: err.message
    });
    console.error(app.formattedTime() + err);
  }
};
