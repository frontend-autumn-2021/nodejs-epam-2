const Note = require('../models/note');
const app = require('../app');

exports.getNotes = async function (req, res) {
  try {
    const { offset, limit } = req.query;
    const user = req.user;

    const notes = await Note.find({ userId: user._id }).skip(offset).limit(limit);
    res.status(200).send({
      offset,
      limit,
      count: notes.length,
      notes
    })
    console.log(app.formattedTime() + 'Get notes by user: ' + user._id);
  } catch (err) {
    res.status(400).send({
      message: err.message
    });
    console.error(app.formattedTime() + err);
  }
};

exports.addNote = async function (req, res) {
  try {
    const noteText = req.body.text;
    const date = new Date();
    const note = new Note({
      userId: req.user._id,
      completed: false,
      text: noteText,
      createdDate: date,
    });

    await note.save();
    res.status(200).send({
      message: 'Success',
    });
    console.log(app.formattedTime() + 'Add note: ' + note._id);
  } catch (err) {
    res.status(400).send({
      message: err.message
    });
    console.error(app.formattedTime() + err);
  }
};

exports.getNoteById = async function (req, res) {
  try {
    const id = req.params.id;

    const note = await Note.findOne({ _id: id, userId: req.user._id });
    if (!note) {
      throw new Error('Note not found');
    }
    res.status(200).send({
      note
    });
    console.log(app.formattedTime() + 'Get note: ' + note._id);
  } catch (err) {
    res.status(400).send({
      message: err.message
    });
    console.error(app.formattedTime() + err);
  }
};

exports.updateNote = async function (req, res) {
  try {
    const id = req.params.id;
    const newText = req.body.text;

    const note = await Note.updateOne(
      { _id: id, userId: req.user._id },
      {
        $set:
        {
          text: newText,
        },
      },
    );
    if (!note) {
      throw new Error('Note not found');
    }
    res.status(200).send({
      message: 'Success',
    });
    console.log(app.formattedTime() + 'Update note: ' + id);
  } catch (err) {
    res.status(400).send({
      message: err.message
    });
    console.error(app.formattedTime() + err);
  }
};

exports.checkNote = async function (req, res) {
  try {
    const id = req.params.id;

    const note = await Note.findOneAndUpdate(
      { _id: id, userId: req.user._id }, [
      {
        $set:
        {
          completed: { $not: '$completed' },
        },
      },
    ]);
    if (!note) {
      throw new Error('Note not found');
    }
    res.status(200).send({
      message: 'Success',
    });
    console.log(app.formattedTime() + 'Check/uncheck note: ' + note._id);
  } catch (err) {
    res.status(400).send({
      message: err.message
    });
    console.error(app.formattedTime() + err);
  }
};

exports.deleteNote = async function (req, res) {
  try {
    const id = req.params.id;

    Note.deleteOne({ _id: id, userId: req.user._id }, (err, result) => {
      if (err || result.deletedCount === 0) {
        throw err || new Error('Note not found');
      }
      res.status(200).send({
        message: 'Success',
      });
      console.log(app.formattedTime() + 'Delete note: ' + id);
    });
  } catch (err) {
    res.status(400).send({
      message: err.message
    });
    console.error(app.formattedTime() + err);
  }
};
